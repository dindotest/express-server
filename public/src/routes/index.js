"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const OneController_1 = __importDefault(require("../controller/OneController"));
const router = (0, express_1.Router)();
router.use('/one', OneController_1.default);
exports.default = router;
