"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const routes_1 = __importDefault(require("./routes"));
const dotenv_1 = __importDefault(require("dotenv"));
const PORT = process.env.PORT || 4040;
dotenv_1.default.config();
const app = (0, express_1.default)();
app.use(express_1.default.json());
app.use((0, cors_1.default)());
// req = data coming in
// res = data from server as response
app.post('/username', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    console.log(reqBody);
    res.status(200).json({
        data: reqBody.username,
    });
}));
console.log(process.env.NODE_ENV);
app.get('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    res.status(200).json({
        data: 'working',
        env: process.env.NODE_ENV,
        variable: process.env.SOME_VARIABLE,
    });
}));
app.use('/api/v1', routes_1.default);
app.get('/variable', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log(process.env);
    res.status(200).json({
        success: 'data',
    });
}));
app.get('/get-example', (_req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('pass');
    res.status(200).json({
        data: 'success',
    });
}));
app.listen(PORT, () => {
    console.log(`server is running in ${PORT}`);
});
