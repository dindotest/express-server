import { Router } from 'express';
import baseRouter from '../controller/BaseController';
import oneRouter from '../controller/OneController';

const router = Router();
router.use('/one', oneRouter);
router.use('/', baseRouter);

export default router;
