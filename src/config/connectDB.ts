import mongoose from 'mongoose';
import config from './config';

const connectDB = async () => {
  const connection = await mongoose.connect(config.MONGO_URI as string);

  console.log(`MongoDB Connected: ${connection.connection.host}`);
};

export default connectDB;
