import dotenv from 'dotenv';

dotenv.config();

console.log(process.env.MONGO_URI);

const MONGO_URI =
  process.env.NODE_ENV === 'development'
    ? process.env.MONGO_URI
    : process.env.MONGO_URI;

const FIREBASE_ADMIN_CONFIG = {
  type: process.env.FIREBASE_ADMIN_TYPE as string,
  projectId: process.env.FIREBASE_ADMIN_PROJECT_ID as string,
  privateKeyId: process.env.FIREBASE_ADMIN_PRIVATE_KEY_ID as string,
  privateKey: process.env.FIREBASE_ADMIN_PRIVATE_KEY as string,
  clientEmail: process.env.FIREBASE_ADMIN_CLIENT_EMAIL as string,
  clientId: process.env.FIREBASE_ADMIN_CLIENT_ID as string,
  authUri: process.env.FIREBASE_ADMIN_AUTH_URI as string,
  tokenUri: process.env.FIREBASE_ADMIN_TOKEN_URI as string,
  authProviderX509CertUrl: process.env
    .FIREBASE_ADMIN_AUTH_PROVIDER_X509_CER_URL as string,
  clientC509CertUrl: process.env.FIREBASE_ADMIN_CLIENT_X509_CERT_URL as string,
};

const XERO_CONFIG = {
  XERO_CLIENT_ID: process.env.XERO_CLIENT_ID as string,
  XERO_CLIENT_SECRET: process.env.XERO_CLIENT_SECRET as string,
  XERO_REDIRECT_URIS: process.env.XERO_REDIRECT_URIS as string,
  XERO_SCOPES: process.env.XERO_SCOPES as string,
};

const GOOGLE_GMAIL_CONFIG = {
  GMAIL_CLIENT_ID: process.env.GMAIL_CLIENT_ID as string,
  GMAIL_CLIENT_SECRET: process.env.GMAIL_CLIENT_SECRET as string,
  GMAIL_REFRESH_TOKEN: process.env.GMAIL_REFRESH_TOKEN as string,
  GMAIL_REDIRECT_URI: process.env.GMAIL_REDIRECT_URI as string,
};

const config = {
  MONGO_URI,
  FIREBASE_ADMIN_CONFIG,
  XERO_CONFIG,
  GOOGLE_GMAIL_CONFIG,
};

export default config;
