import express, { Request, Response } from 'express';
import cors from 'cors';
import router from './routes';
import dotenv from 'dotenv';
import connectDB from './config/connectDB';

const PORT = process.env.PORT || 4040;

dotenv.config();

connectDB();

const app = express();

app.use(express.json());

app.use(cors());

// req = data coming in
// res = data from server as response
app.post('/username', async (req, res) => {
  const reqBody = req.body;

  console.log(reqBody);

  res.status(200).json({
    data: reqBody.username,
  });
});

console.log(process.env.NODE_ENV);

app.get('/', async (req: Request, res: Response) => {
  res.status(200).json({
    data: 'working',
    env: process.env.NODE_ENV,
    variable: process.env.SOME_VARIABLE,
  });
});

app.use('/server', router);

app.get('/variable', async (req: Request, res: Response) => {
  console.log(process.env);

  res.status(200).json({
    success: 'data',
  });
});

app.get('/get-example', async (_req: Request, res: Response) => {
  console.log('pass');
  res.status(200).json({
    data: 'success',
  });
});

app.listen(PORT, () => {
  console.log(`server is running in ${PORT}`);
});
