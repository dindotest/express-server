import { NextFunction, Router, Response, Request } from 'express';
import dotenv from 'dotenv';

dotenv.config();

const oneRouter = Router();

oneRouter
  .route('/two')
  .get(async (_req: Request, res: Response, next: NextFunction) => {
    try {
      res.status(200).json({
        data: 'two',
        env: process.env.NODE_ENV,
        envData: process.env.SOME_VARIABLE,
        key: process.env.KEY,
      });
    } catch (e) {
      next(e);
    }
  });

export default oneRouter;
