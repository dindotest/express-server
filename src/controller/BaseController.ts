import { Router, Request, Response, NextFunction } from 'express';
import EmailTemplateModel from '../models/EmailTemplates';
import JobModel from '../models/JobModel';

const baseRouter = Router();

baseRouter
  .route('/test')
  .get(async (req: Request, res: Response, next: NextFunction) => {
    try {
      const allJobsArray = await JobModel.find();
      console.log(allJobsArray);

      res.status(200).json({
        data: allJobsArray,
      });
    } catch (e) {
      next(e);
    }
  });

baseRouter
  .route('/email-templates')
  .get(async (req: Request, res: Response, next: NextFunction) => {
    try {
      const emailTemplates = await EmailTemplateModel.find();

      res.status(200).json({
        success: true,
        data: emailTemplates,
      });
    } catch (e) {
      next(e);
    }
  })
  .post(async (req: Request, res: Response, next: NextFunction) => {
    try {
      const reqBody = req.body as { message: 'Hello post request' };
      res.status(200).json({
        success: true,
        data: reqBody.message,
      });
    } catch (e) {
      next(e);
    }
  });

export default baseRouter;
