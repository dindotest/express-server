import mongoose from 'mongoose';
import EmailTemplateInterface from './EmailTemplatesInterface';

const EmailTemplateSchema = new mongoose.Schema<EmailTemplateInterface>(
  {
    companyId: {
      type: String,
      required: [true, `Company data required`],
      ref: 'Company',
    },
    templateName: {
      type: String,
      required: [true, 'Must provide template name'],
    },
    subject: {
      type: String,
      required: [true, 'Must provide subject'],
    },
    message: {
      type: String,
      required: [true, `Message must not be empty`],
    },
    requestSignature: {
      type: Boolean,
      required: [true, 'Must provide request signature value'],
      default: false,
    },
    createdAt: {
      type: Date,
    },
    updatedAt: {
      type: Date,
    },
  },
  { timestamps: true }
);

const EmailTemplateModel = mongoose.model<EmailTemplateInterface>(
  'EmailTemplate',
  EmailTemplateSchema
);

export default EmailTemplateModel;
