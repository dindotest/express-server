interface EmailTemplateInterface {
  _id?: string;
  companyId: string;
  templateName: string;
  subject: string;
  message: string;
  requestSignature: boolean;

  // log
  createdAt: Date;
  updatedAt: Date;
}

export default EmailTemplateInterface;
