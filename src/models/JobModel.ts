import mongoose from 'mongoose';
import JobInterface from './JobInterface';

const JobSchema = new mongoose.Schema<JobInterface>(
  {
    companyId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Company',
      required: [true, 'Please add a company Id'],
    },
    jobTitle: {
      type: String,
    },
    createdAt: {
      type: Date,
    },
    updatedAt: {
      type: Date,
    },
  },
  { timestamps: true }
);

const JobModel = mongoose.model<JobInterface>('Job', JobSchema);

export default JobModel;
