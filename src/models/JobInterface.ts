interface JobInterface {
  _id?: string;
  companyId?: string;
  jobTitle: string;

  // log
  createdAt: Date;
  updatedAt: Date;
}

export default JobInterface;
